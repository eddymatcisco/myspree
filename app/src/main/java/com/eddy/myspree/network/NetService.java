package com.eddy.myspree.network;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import rx.Observable;

public interface NetService {
    String SERVICE_ENDPOINT = "https://api-dev.spreeza.net/api/v1/catalog/browse/";
    String SERVICE_ENDPOINT_SEARCH = "https://api-dev.spreeza.net/api/v1/catalog/";

    @Streaming
    @GET("2?p=1")
    Observable<ResponseBody> getProductList();

    @Streaming
    @GET("search?")
    Observable<ResponseBody> getSearchProductList(@Query("q") String keyword, @Query("cat") int cat,@Query("p") int page);
}
