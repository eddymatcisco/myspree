
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("regular")
    @Expose
    private Integer regular;
    @SerializedName("selling")
    @Expose
    private Integer selling;
    @SerializedName("special_price")
    @Expose
    private Integer specialPrice;
    @SerializedName("special_from_date")
    @Expose
    private String specialFromDate;
    @SerializedName("special_to_date")
    @Expose
    private String specialToDate;

    public Integer getRegular() {
        return regular;
    }

    public void setRegular(Integer regular) {
        this.regular = regular;
    }

    public Integer getSelling() {
        return selling;
    }

    public void setSelling(Integer selling) {
        this.selling = selling;
    }

    public Integer getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(Integer specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getSpecialFromDate() {
        return specialFromDate;
    }

    public void setSpecialFromDate(String specialFromDate) {
        this.specialFromDate = specialFromDate;
    }

    public String getSpecialToDate() {
        return specialToDate;
    }

    public void setSpecialToDate(String specialToDate) {
        this.specialToDate = specialToDate;
    }

}
