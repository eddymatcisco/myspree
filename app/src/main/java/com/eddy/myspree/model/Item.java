
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    @SerializedName("facets")
    @Expose
    private Facets facets;
    @SerializedName("category")
    @Expose
    private Category_ category;
    @SerializedName("default_sort_by")
    @Expose
    private DefaultSortBy defaultSortBy;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Facets getFacets() {
        return facets;
    }

    public void setFacets(Facets facets) {
        this.facets = facets;
    }

    public Category_ getCategory() {
        return category;
    }

    public void setCategory(Category_ category) {
        this.category = category;
    }

    public DefaultSortBy getDefaultSortBy() {
        return defaultSortBy;
    }

    public void setDefaultSortBy(DefaultSortBy defaultSortBy) {
        this.defaultSortBy = defaultSortBy;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
