package com.eddy.myspree.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.eddy.myspree.R;
import com.eddy.myspree.model.Product;
import com.eddy.myspree.network.NetService;
import com.eddy.myspree.network.RestUtils;
import com.eddy.myspree.util.LogUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.observables.SyncOnSubscribe;
import rx.plugins.RxJavaHooks;
import rx.schedulers.Schedulers;



public class ProductListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */

    private List<Product> mProduct = new ArrayList<>();
    private boolean mTwoPane;
    private View recyclerView;
    private CardAdapter cardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setLogo(R.mipmap.ic_launcher);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        cardAdapter = new CardAdapter(mProduct, getApplicationContext());
        playJsonSample();

        Log.d("SPREEJOB", "Product size check: "+mProduct.size());

        recyclerView = findViewById(R.id.product_list);
        assert recyclerView != null;

        setupRecyclerView((RecyclerView) recyclerView, getApplicationContext());

        if (findViewById(R.id.product_detail_container) != null) {

            mTwoPane = true;
        }

    }

    public void playJsonSample() {
        Log.d("SPREEJOB", "starting");

       NetService JsonApi = RestUtils.createService(NetService.class, NetService.SERVICE_ENDPOINT);

        final int[] counter = {0};
        Gson gson = new GsonBuilder().create();

        JsonApi.getProductList()
                .flatMap(responseBody -> convertObjectsStream(responseBody, gson, Product.class))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Product>() {
                    @Override
                    public void onStart() {
                        super.onStart();

                        request(1);
                    }

                    @Override
                    public void onNext(Product product) {
                        counter[0]++;

                        Log.d("SPREEJOB", "Product size: "+mProduct.size());
                        Log.d("SPREEJOB", product.getTitle());
                        cardAdapter.addData(product);
                        request(1);
                    }

                    @Override
                    public void onCompleted() {
                        Log.d("SPREEJOB", "completed");
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d("SPREEJOB", e.getMessage());

                    }
                });
    }

    public void playJsonSearch(String key) {
        Log.d("SPREEJOB", "starting");

        NetService JsonApi = RestUtils.createService(NetService.class, NetService.SERVICE_ENDPOINT_SEARCH);

        final int[] counter = {0};
        Gson gson = new GsonBuilder().create();

        JsonApi.getSearchProductList(key,2,1)
                .flatMap(responseBody -> convertObjectsStream(responseBody, gson, Product.class))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Product>() {
                    @Override
                    public void onStart() {
                        super.onStart();

                        request(1);
                    }

                    @Override
                    public void onNext(Product product) {
                        counter[0]++;

                        Log.d("SPREEJOB", "Product size: "+mProduct.size());
                        Log.d("SPREEJOB", product.getTitle());
                        cardAdapter.addData(product);
                        request(1);
                    }

                    @Override
                    public void onCompleted() {
                        Log.d("SPREEJOB", "completed");
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d("SPREEJOB", e.getMessage());

                    }
                });
    }

    @NonNull
    private static <TYPE> Observable<TYPE> convertObjectsStream(ResponseBody responseBody, Gson gson, Class<TYPE> clazz) {
        Type type = TypeToken.get(clazz).getType();
        return Observable.create(SyncOnSubscribe.<JsonReader, TYPE>createStateful(
                () -> {
                    try {
                        JsonReader reader = gson.newJsonReader(responseBody.charStream());
                        reader.beginObject();
                        // looking for a "features" field with actual array of elements
                        while (reader.hasNext()) {
                            // the array begins at json-field "features"
                            if (reader.nextName().equals("products")) {
                                reader.beginArray();
                                return reader;
                            }
                            reader.skipValue();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        RxJavaHooks.onError(e);
                    }
                    return null;
                },
                (reader, observer) -> {

                    if (reader == null) {
                        observer.onCompleted();
                        return null;
                    }

                    try {
                        if (reader.hasNext()) {
                            TYPE t = gson.fromJson(reader, type);
                            observer.onNext(t);
                        } else {
                            observer.onCompleted();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        observer.onError(e);
                    }

                    return reader;
                }
                , Util::closeQuietly)
        );
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView , Context context) {
        recyclerView.setLayoutManager(new GridLayoutManager(context,2));
        recyclerView.setAdapter(cardAdapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        playJsonSearch(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        final List<Product> filteredModelList = filter(cardAdapter.getList(), newText);
        cardAdapter.clear();
        cardAdapter.setFilter(filteredModelList);
        return true;

    }

    private List<Product> filter(List<Product> models, String query) {
        query = query.toLowerCase();final List<Product> filteredModelList = new ArrayList<>();
        for (Product model : models) {
            final String text = model.getTitle().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public class CardAdapter
            extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

        private List<Product> mValues;
        Context mContext;

        public void addData(Product product) {
            mValues.add(product);
            notifyDataSetChanged();
        }

        public void setFilter(List<Product> mp){
            mValues = mp;
            notifyDataSetChanged();
        }

        public void clear() {
            mValues.clear();
            notifyDataSetChanged();
        }

        public CardAdapter(List<Product> items,Context context) {
            mValues = items;
            mContext=context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mTitle.setText(mValues.get(position).getTitle());
            holder.mPrice.setText("R "+mValues.get(position).getPrice().getSpecialPrice());
//            Glide.with(holder.mView.getContext()).load(mValues.get(position).getPics())
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    //.placeholder(R.drawable.defaultplaceholder )
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(holder.mImage);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(ProductDetailFragment.ARG_ITEM_SKU, holder.mItem.getSku());
                        arguments.putString(ProductDetailFragment.ARG_ITEM_TITLE, holder.mItem.getTitle());
                        arguments.putString(ProductDetailFragment.ARG_ITEM_PRICE, "R "+(holder.mItem.getPrice()).getSpecialPrice());
                        arguments.putString(ProductDetailFragment.ARG_ITEM_DETAIL, String.valueOf(holder.mItem.getDetail()));
                        arguments.putString(ProductDetailFragment.ARG_ITEM_PIC, String.valueOf(holder.mItem.getPics()));
                        ProductDetailFragment fragment = new ProductDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.product_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        intent.putExtra(ProductDetailFragment.ARG_ITEM_SKU, holder.mItem.getSku());
                        intent.putExtra(ProductDetailFragment.ARG_ITEM_TITLE, holder.mItem.getTitle());
                        intent.putExtra(ProductDetailFragment.ARG_ITEM_PRICE, "R "+(holder.mItem.getPrice()).getSpecialPrice());
                        intent.putExtra(ProductDetailFragment.ARG_ITEM_DETAIL, String.valueOf(holder.mItem.getDetail()));
                        intent.putExtra(ProductDetailFragment.ARG_ITEM_PIC, String.valueOf(holder.mItem.getPics()));

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public List<Product> getList() {
            return mValues;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mTitle;
            public final TextView mPrice;
            public final ImageView mImage;
            public Product mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mTitle = (TextView) view.findViewById(R.id.title);
                mPrice = (TextView) view.findViewById(R.id.price);
                mImage = (ImageView) view.findViewById(R.id.image);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTitle.getText() + "'";
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        //a.setFilter(Product);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {

                        return true; // Return true to expand action view
                    }
                });
        return true;

    }
}
