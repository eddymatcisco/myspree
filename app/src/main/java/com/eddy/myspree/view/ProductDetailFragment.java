package com.eddy.myspree.view;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.eddy.myspree.R;


public class ProductDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */

    public static final String ARG_ITEM_TITLE = "item_title" ;
    public static final String ARG_ITEM_SKU = "item_sku";
    public static final String ARG_ITEM_PRICE = "item_price";
    public static final String ARG_ITEM_DETAIL = "item_detail";
    public static final String ARG_ITEM_PIC = "item_pic";


    public ProductDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_SKU)) {

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(getArguments().getString(ARG_ITEM_TITLE));
        }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.product_detail, container, false);

        if (getArguments().containsKey(ARG_ITEM_DETAIL)) {
            ((TextView) rootView.findViewById(R.id.mPrice)).setText(getArguments().getString(ARG_ITEM_PRICE));
            ((WebView) rootView.findViewById(R.id.product_detail)).loadData(getArguments().getString(ARG_ITEM_DETAIL),"text/html",null);

        }

        return rootView;
    }
}
