
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {

    @SerializedName("entity_id")
    @Expose
    private Integer entityId;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("gtin")
    @Expose
    private Object gtin;
    @SerializedName("mpn")
    @Expose
    private String mpn;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("why_buy")
    @Expose
    private String whyBuy;
    @SerializedName("why_buy_plain")
    @Expose
    private String whyBuyPlain;
    @SerializedName("go_live")
    @Expose
    private String goLive;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("colour")
    @Expose
    private Colour colour;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("pics")
    @Expose
    private Pics pics;
    @SerializedName("measurement_image")
    @Expose
    private MeasurementImage measurementImage;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("visibility")
    @Expose
    private Integer visibility;
    @SerializedName("coming_soon")
    @Expose
    private Integer comingSoon;
    @SerializedName("editors_pick")
    @Expose
    private Integer editorsPick;
    @SerializedName("must_have")
    @Expose
    private Integer mustHave;
    @SerializedName("is_new")
    @Expose
    private Integer isNew;
    @SerializedName("show_size_chart")
    @Expose
    private Integer showSizeChart;
    @SerializedName("detail_url")
    @Expose
    private String detailUrl;
    @SerializedName("simples")
    @Expose
    private List<Simple> simples = null;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("department")
    @Expose
    private Department department;
    @SerializedName("demographic")
    @Expose
    private String demographic;
    @SerializedName("ribbon")
    @Expose
    private Object ribbon;

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Object getGtin() {
        return gtin;
    }

    public void setGtin(Object gtin) {
        this.gtin = gtin;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getWhyBuy() {
        return whyBuy;
    }

    public void setWhyBuy(String whyBuy) {
        this.whyBuy = whyBuy;
    }

    public String getWhyBuyPlain() {
        return whyBuyPlain;
    }

    public void setWhyBuyPlain(String whyBuyPlain) {
        this.whyBuyPlain = whyBuyPlain;
    }

    public String getGoLive() {
        return goLive;
    }

    public void setGoLive(String goLive) {
        this.goLive = goLive;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Pics getPics() {
        return pics;
    }

    public void setPics(Pics pics) {
        this.pics = pics;
    }

    public MeasurementImage getMeasurementImage() {
        return measurementImage;
    }

    public void setMeasurementImage(MeasurementImage measurementImage) {
        this.measurementImage = measurementImage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public Integer getComingSoon() {
        return comingSoon;
    }

    public void setComingSoon(Integer comingSoon) {
        this.comingSoon = comingSoon;
    }

    public Integer getEditorsPick() {
        return editorsPick;
    }

    public void setEditorsPick(Integer editorsPick) {
        this.editorsPick = editorsPick;
    }

    public Integer getMustHave() {
        return mustHave;
    }

    public void setMustHave(Integer mustHave) {
        this.mustHave = mustHave;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public Integer getShowSizeChart() {
        return showSizeChart;
    }

    public void setShowSizeChart(Integer showSizeChart) {
        this.showSizeChart = showSizeChart;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public List<Simple> getSimples() {
        return simples;
    }

    public void setSimples(List<Simple> simples) {
        this.simples = simples;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getDemographic() {
        return demographic;
    }

    public void setDemographic(String demographic) {
        this.demographic = demographic;
    }

    public Object getRibbon() {
        return ribbon;
    }

    public void setRibbon(Object ribbon) {
        this.ribbon = ribbon;
    }

}
