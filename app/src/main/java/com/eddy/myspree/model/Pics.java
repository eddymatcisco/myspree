
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pics {

    @SerializedName("small")
    @Expose
    private String small;
    @SerializedName("hover")
    @Expose
    private String hover;

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getHover() {
        return hover;
    }

    public void setHover(String hover) {
        this.hover = hover;
    }

}
