
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MeasurementImage {

    @SerializedName("show_measurement_image")
    @Expose
    private Integer showMeasurementImage;
    @SerializedName("measurement_image_src")
    @Expose
    private String measurementImageSrc;

    public Integer getShowMeasurementImage() {
        return showMeasurementImage;
    }

    public void setShowMeasurementImage(Integer showMeasurementImage) {
        this.showMeasurementImage = showMeasurementImage;
    }

    public String getMeasurementImageSrc() {
        return measurementImageSrc;
    }

    public void setMeasurementImageSrc(String measurementImageSrc) {
        this.measurementImageSrc = measurementImageSrc;
    }

}
