
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Simple {

    @SerializedName("entity_id")
    @Expose
    private Integer entityId;
    @SerializedName("sku")
    @Expose
    private String sku;
//    @SerializedName("size_id")
//    @Expose
//    private Integer sizeId;
    @SerializedName("size_value")
    @Expose
    private String sizeValue;
    @SerializedName("stock_qty")
    @Expose
    private Integer stockQty;
    @SerializedName("stock_status")
    @Expose
    private Integer stockStatus;
    @SerializedName("size_order")
    @Expose
    private Integer sizeOrder;
    @SerializedName("supplier_product_code")
    @Expose
    private String supplierProductCode;
    @SerializedName("agg_key")
    @Expose
    private String aggKey;
    @SerializedName("size2_id")
    @Expose
    private String size2Id;
    @SerializedName("size2_group")
    @Expose
    private String size2Group;
    @SerializedName("size2_order")
    @Expose
    private Integer size2Order;
    @SerializedName("size2_agg_key")
    @Expose
    private String size2AggKey;

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

//    public Integer getSizeId() {
//        return sizeId;
//    }
//
//    public void setSizeId(Integer sizeId) {
//        this.sizeId = sizeId;
//    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public Integer getStockQty() {
        return stockQty;
    }

    public void setStockQty(Integer stockQty) {
        this.stockQty = stockQty;
    }

    public Integer getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(Integer stockStatus) {
        this.stockStatus = stockStatus;
    }

    public Integer getSizeOrder() {
        return sizeOrder;
    }

    public void setSizeOrder(Integer sizeOrder) {
        this.sizeOrder = sizeOrder;
    }

    public String getSupplierProductCode() {
        return supplierProductCode;
    }

    public void setSupplierProductCode(String supplierProductCode) {
        this.supplierProductCode = supplierProductCode;
    }

    public String getAggKey() {
        return aggKey;
    }

    public void setAggKey(String aggKey) {
        this.aggKey = aggKey;
    }

    public String getSize2Id() {
        return size2Id;
    }

    public void setSize2Id(String size2Id) {
        this.size2Id = size2Id;
    }

    public String getSize2Group() {
        return size2Group;
    }

    public void setSize2Group(String size2Group) {
        this.size2Group = size2Group;
    }

    public Integer getSize2Order() {
        return size2Order;
    }

    public void setSize2Order(Integer size2Order) {
        this.size2Order = size2Order;
    }

    public String getSize2AggKey() {
        return size2AggKey;
    }

    public void setSize2AggKey(String size2AggKey) {
        this.size2AggKey = size2AggKey;
    }

}
