
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Facets {

    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("sizes2")
    @Expose
    private List<Sizes2> sizes2 = null;
    @SerializedName("prices")
    @Expose
    private List<Price_> prices = null;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Sizes2> getSizes2() {
        return sizes2;
    }

    public void setSizes2(List<Sizes2> sizes2) {
        this.sizes2 = sizes2;
    }

    public List<Price_> getPrices() {
        return prices;
    }

    public void setPrices(List<Price_> prices) {
        this.prices = prices;
    }

}
