
package com.eddy.myspree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Brand {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type_code")
    @Expose
    private String typeCode;
    @SerializedName("type_value")
    @Expose
    private String typeValue;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("vanity_uri")
    @Expose
    private String vanityUri;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("desktop_image")
    @Expose
    private String desktopImage;
    @SerializedName("mobile_image")
    @Expose
    private String mobileImage;
    @SerializedName("app_image")
    @Expose
    private String appImage;
    @SerializedName("app_logo")
    @Expose
    private String appLogo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVanityUri() {
        return vanityUri;
    }

    public void setVanityUri(String vanityUri) {
        this.vanityUri = vanityUri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDesktopImage() {
        return desktopImage;
    }

    public void setDesktopImage(String desktopImage) {
        this.desktopImage = desktopImage;
    }

    public String getMobileImage() {
        return mobileImage;
    }

    public void setMobileImage(String mobileImage) {
        this.mobileImage = mobileImage;
    }

    public String getAppImage() {
        return appImage;
    }

    public void setAppImage(String appImage) {
        this.appImage = appImage;
    }

    public String getAppLogo() {
        return appLogo;
    }

    public void setAppLogo(String appLogo) {
        this.appLogo = appLogo;
    }

}
